
MATÉRIEL LIBRE TEXTES DE RÉFÉRENCES / textes cadres relatifs à la notion
de matériel libre selon le projet materiel-libre

antoine c

Note : ce fichier README est réalisé initialement avec le logiciel de
traitement de texte de la suite LibreOffice. Le fichier .odt est ensuite
converti en fichier markdown .md. Pour l’améliorer, il faut réaliser les
changements dans le fichier README-source.odt, puis convertir en fichier
README.md

MATÉRIEL LIBRE TEXTES DE RÉFÉRENCES
===================================

Ce contenu fait partie d’un [projet global et
expérimental](https://framagit.org/materiel-libre) relatif à la [notion
de matériel
libre](https://www.gnu.org/philosophy/free-hardware-designs.fr.html),
développé dans un esprit de [recherche
appliquée](https://fr.wikipedia.org/wiki/Recherche_appliquée). Ce
[projet global](https://framagit.org/materiel-libre) se développe au
travers, mais aussi en dehors, des [dépôts git du groupe
materiel-libre](https://framagit.org/materiel-libre) disponibles sur
[Framagit](https://framagit.org/) - [une instance gitlab
CE](https://gitlab.com/gitlab-org/gitlab-ce/) proposée par
[Framasoft](https://framasoft.org/). [L'association
Labomedia](https://labomedia.org/) est l'une des parties prenantes
principales de ce projet.

Liens directs vers des contenus « phares » :

**|** [**Travaux en cours**](../travaux-en-cours.md) **|**

**|**
[**Wiki**](https://framagit.org/materiel-libre/materiel-libre-textes-de-reference/wikis/home)
**|** [**Doc**](https://framagit.org/materiel-libre) **|**
[**Versions**](https://framagit.org/materiel-libre/materiel-libre-textes-de-reference/-/releases)
**|**

Bienvenue !
-----------

Ce projet est « ouvert » à toutes et tous. Toute personne est la
bienvenue, quelques-soient ses bagages ou son niveau de compétences.

Description
-----------

### Types de travaux réalisés

-   Exploration, expérimentation, recherche, développement, production,
    de textes cadres qui permettent de faire de la notion de matériel
    libre, une réalité.

### Ce qui se fait ici

-   [Discussions à
    distance](https://framagit.org/materiel-libre/materiel-libre-textes-de-reference/issues)
-   [Production participative de contenus
    expérimentaux](https://framagit.org/materiel-libre/materiel-libre-textes-de-reference)

### Ce que fait ce contenu, ce qu’il délivre

-   des réflexions sur la notion de « Matériel Libre ».
-   des textes définissant la notion de « Matériel Libre »
-   des textes expérimentaux de termes et conditions encadrant la
    fabrication de Matériel Libres

### Utilité

-   Participe à faire avancer les réflexions et les réalisations
    concernant la notion de matériel libre

### Buts

-   publier des contenus qui peuvent avoir une utilité pour le
    développement de la notion de matériel libre

### Motivations initiales

-   Trouver un cadre juridique pour arriver à fabriquer des objets en
    les mettant à disposition sous licence libre, comme cela se fait
    pour les logiciels libres ou pour la documentation libre.

### Intentions de départ

-   Produire des termes et conditions de type « licence libre »,
    utilisables pour les fabrications de choses matérielles

Statut
------

Statut par rapport à l’indicateur « Niveau de Maturité Technologique
NMT » ([1](https://fr.wikipedia.org/wiki/Technology_readiness_level))
([2](https://www.ic.gc.ca/eic/site/ito-oti.nsf/fra/00849.html)) ou
« Technology Readiness Level TRL »
([3](https://en.wikipedia.org/wiki/Technology_readiness_level))
([4](https://ec.europa.eu/research/participants/data/ref/h2020/wp/2014_2015/annexes/h2020-wp1415-annex-g-trl_en.pdf))

-   Niveau de Maturité Technologique atteint : [TLR - 4 : « Validation
    en
    laboratoire »](https://fr.wikipedia.org/wiki/Technology_readiness_level)

Savoir faire requis
-------------------

-   Savoir ce qu’est une licence libre
-   Avoir lu et compris la documentation source existant sur le matériel
    libre (FSF, wikipedia, OSHWA, CERN-OHL
-   Avoir lu et compris les différentes licences OHL (CERN-OHL, TAPR,
    ...)
-   Avoir été impliqué dans la pratique de la fabrication d’objets dans
    la vie réelle à des fins d’exploitation artisanales, industrielles
    et commerciales

Par où commencer
----------------

-   Consulter les [travaux existants (réalisés ou en
    cours)](https://framagit.org/materiel-libre/materiel-libre-textes-de-reference/-/branches)[et
    les discussions en
    cours](https://framagit.org/materiel-libre/materiel-libre-textes-de-reference/-/branches)
-   [Lancer une discussion sur une amélioration ou un
    questionnement](https://framagit.org/materiel-libre/materiel-libre-textes-de-reference/issues)(poster
    une issue)

Trouver de l’aide
-----------------

-   [Support](https://framagit.org/materiel-libre)
-   [Contact](https://framagit.org/groups/materiel-libre/-/group_members)

Qui maintient ce projet
-----------------------

-   [groupe materiel-libre](https://framagit.org/materiel-libre)

Visuels
-------

-   FIXME ! → [proposez un
    visuel](https://framagit.org/materiel-libre/materiel-libre-textes-de-reference/issues)

Usages
------

-   études et recherches sur la notion de matériel libre
-   vulgarisation de la connaissance sur la notion de matériel libre
-   expérimentations de textes cadres sur la notion de matériel libre
-   études et recherches d’étalonnage par rapport à une notion référente
    de matériel libre

Remonter un problème
--------------------

-   [Contact pour les problèmes](https://framagit.org/Antoine) ou
    [postez un
    messag](https://framagit.org/materiel-libre/materiel-libre-textes-de-reference/issues)[e
    explicite sur
    framagit](https://framagit.org/materiel-libre/materiel-libre-textes-de-reference/issues)

Informations complémentaires
----------------------------

### Feuille de route

-   Publication des 10 caractéristiques sur des sites tiers dédiés
-   Traduction en anglais des 10 caractéristiques de la version 19.01.2
    et ultérieures, et publication de la traduction
-   Reprise de la production \[fr\] de la licence matériel libre,
    publication, traduction \[en\] puis publication \[en\]
-   Amélioration permanente

### Travaux en cours sur cette instance « matériel-libre »

-   [**Travaux en cours**](../travaux-en-cours.md)

### Contribuer

-   Qui peut contribuer : Tout le monde, c’est ouvert. Voir plus haut la
    section « savoirs faire requis »
-   [Guide
    pour](https://framagit.org/materiel-libre/materiel-libre-comment-contribuer)[contribuer](https://framagit.org/materiel-libre/materiel-libre-comment-contribuer)[et
    pratiques
    contributives](https://framagit.org/materiel-libre/materiel-libre-comment-contribuer)

### Connaissances et savoirs mis en jeu

-   [Licences libres](https://www.gnu.org/licenses/license-list.fr.html)

### Exemple de contribution

-   [Texte des 10 caractéristiques d’un matériel
    libre](../../../../../LES-10-CARACTERISTIQUES-MATERIEL-LIBRE/fichier-source-odt-10-CARACTERISTIQUES-MATERIEL-LIBRE.odt)

### Utilisation des contenus

-   [Mode d'emploi](http://utilisation.md/)
-   [U](http://utilisation.md/)[tilisation (comment
    utiliser)](http://utilisation.md/)
-   [P](http://utilisation.md/)[ré-requis à
    l'utilisation](http://utilisation.md/)
-   [L](http://utilisation.md/)[imitation à
    l'utilisation](http://utilisation.md/)
-   [P](http://utilisation.md/)[ourquoi
    utiliser](http://utilisation.md/)

### Tests des contenus

-   Non renseigné (FIXME!)

### Ressources documentaires de cette instance « matériel-libre »

-   [Do](https://framagit.org/materiel-libre)[cumentation](https://framagit.org/materiel-libre)
-   [Wiki](https://framagit.org/materiel-libre/materiel-libre-textes-de-reference/wikis/home)
-   Autres ressources (FIXME ! )

### Versions concernant cette instance « matériel-libre »

-   [Versions](https://framagit.org/materiel-libre/materiel-libre-textes-de-reference/-/releases)

### À propos

-   <https://framagit.org/materiel-libre/materiel-libre-textes-de-reference>
-   <https://framagit.org/materiel-libre>

### Auteurs, crédits concernant les contenus publiés sur cette instance « matériel-libre »

-   [Groupe materiel-libre sur
    Framagit](https://framagit.org/materiel-libre)
-   [Antoine C](https://framagit.org/Antoine) (« initial founder »,
    « fondateur initial »)

### Licence

-   Les contenus mis à disposition sous droit restreints, comportent une
    indication en ce sens.

### Remerciements

-   L[es personnes humaines du groupe
    materiel-libre](https://framagit.org/materiel-libre)
-   la [FSF](https://www.fsf.org/fr) et le projet
    [gnu](https://www.gnu.org/)
-   l’[Open Hardware Repository
    du](https://ohwr.org/welcome)[CERN](https://ohwr.org/welcome)
-   l’association merveilleuse [LABOMEDIA](https://labomedia.org/) et
    ses incroyables OAVL
-   [copyleft attitude et la licence art libre](https://artlibre.org/)
-   [CREATIVE COMMONS](https://creativecommons.org/)
-   et toutes les nombreuses fantastiques personnes qui contribuent à
    l’évolution de l’humanité dans une vision humaniste, pacifique et
    équilibrée des choses

### Autres informations

-   « Quand y’en a plus, y’en a encore » (Roger, le 10 avril de l’an
    d’avant, accoudé au bar en buvant une coudée)

### Notes

-   \[1\] : FL-OS pour Free Libre OpenSource
