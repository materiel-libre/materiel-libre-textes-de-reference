Info: ceci est un fichier markdown. 

Note de version

Version 19.07

Version précédente: 19.01

Changements majeurs:

  * Ajout des "dispositifs immatériels" dans la caractéristique numéro 8

Changements mineurs:

  * ré-aménagement des titres
  * précisions des liens de versions
  * modification de mises en page

Changements relatifs au dépôt git

  * création d'un dépôt spécifique "textes-de-references"
  * modification de l'arborescence
  * fichier source = un fichier .odt
  * fichiers sous autres formats = exports depuis le fichier .odt, ou bien convertissages via le logiciel pandoc (notament pour les fichiers markdown .md)
