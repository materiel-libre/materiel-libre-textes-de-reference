# Travaux en cours et choses à faire

## Travaux en cours
S'il existe des travaux en cours, alors ils se trouvent dans les "branches" du projet qui ne sont ni la branche "master", ni la branche "develop".

S'il n'existe pas de "branche" qui ne sont ni "master" ni "develop", alors c'est qu'il n'existe pas de travaux en cours.

Pour consulter les travaux en cours, il suffit de consulter les "branches" et leurs contenus. 

Pour comprendre ce que sont les notions de branches ainsi que la convention de nommage des branches, [consultez les instructions git pour le groupe materiel-libre](https://framagit.org/materiel-libre/materiel-libre-comment-contribuer).

### Consulter la liste des travaux en cours (liste des branches)

  * [Liste des travaux en cours sur l'interface framagit](https://framagit.org/materiel-libre/materiel-libre-textes-de-reference/-/branches)
  * Liste des travaux en cours en ligne de commande: [se référer aux instructions git pour le groupe  materiel-libre pour créer (cloner) un dépôt sur votre ordinateur local](https://framagit.org/materiel-libre/materiel-libre-comment-contribuer) , puis ensuite faire: `git branch -a`

## Choses à faire / feuille de route

  * Valider les choses en développement sur la branch `develop` pour en faire une version 19.07
  * Travailler sur une nouvelle version, qui inclurait une onzième caractéristique: "dont tout dispositif de la chaîne de valeur, doit être libre".  Cela fait référence à: *tout dispositif matériel ou immatériel d'assemblage, de stockage, de transport, d'installation, de maintenance, de mise au rebus, etc ... lié à la chose fabriquée, doit être libre.*